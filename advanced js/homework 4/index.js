/*
AJAX дозволяє веб-сторінці взаємодіяти з сервером асинхронно, тобто без необхідності перезавантаження сторінки. Головна ідея AJAX полягає в тому, щоб виконувати запити до сервера і отримувати від нього дані в фоновому режимі, під час роботи з веб-сторінкою.
*/


const container = document.querySelector(".container")

fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(res => res.json())
    .then(data => {
       
        data.forEach(({characters, episodeId, name, id, openingCrawl}) => {

            container.insertAdjacentHTML("beforeend", `
                                <div class="movie">
                                <p class="episode">episode:${episodeId}</p>
                                <h4>${name}</h4>
                                    <ul class="class-${id}">
                               
                                    </ul>
                                <p>${openingCrawl}</p>
                                </div>    
                              `);

            let clasId = document.querySelector(`.class-${id}`);


            characters.forEach(el => {
                fetch(el)
                    .then(res => res.json())
                    .then(data => {
                        clasId.insertAdjacentHTML("beforeend", `<li>${data.name}</li>`);
                    });

            });
        });
        
    })
    .catch(err => console.log(err))




