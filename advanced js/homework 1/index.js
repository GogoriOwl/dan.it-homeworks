"use strict"

/*
1.це особливість мови, яка дозволяє об'єктам використовувати властивості та методи інших об'єктів через їх прототипи. Кожен об'єкт в JavaScript може мати прототип, який визначає набір властивостей і методів, що він може успадковувати. Якщо об'єкт не має власної властивості або методу, JavaScript автоматично шукає їх в його прототипі.

2.super() використовується для виклику конструктора батьківського класу всередині конструктора дочірнього класу. Це дозволяє нащадку отримати доступ до властивостей та методів батьківського класу, і виконати необхідну ініціалізацію перед тим, як додати свої власні властивості.

*/

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  getName(){
    return this.name;
  }

  setName(newName){
    if(newName.length > 1){
      this.name = newName
    } else{
      return
    }
  }

  getAge(){
    return this.age;
  }

  setAge(newAge){
    if(newAge > 18 && newAge < 70){
      this.age = newAge
    } else{
      return
    }
  }

  getSalary(){
    return this.salary;
  }

  setSalary(newSalary){
    if(newSalary > 0){
      this.salary = newSalary
    } else{
      return
    }
  }

}

class Programmer extends Employee {
  constructor(lang, name, age, salary) {
    super(name, age, salary);
    this.lang = lang;
  }

  getSalary(){
    return this.salary*3
  }
}

const john = new Programmer("Eng", "John", 23, 10000);
const linn = new Programmer("French", "Linn", 23, 13000);
const michael = new Programmer("Spanich", "Michael", 24, 12500);

console.log(john.getSalary());
console.log(john);
console.log(linn);
console.log(michael);


