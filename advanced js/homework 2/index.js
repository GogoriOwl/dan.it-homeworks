"use strict"

/*
1.Парсинг JSON
Виклики API
Використання сторонніх бібліотек
*/

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

const listContainer = document.querySelector("#root");

class EmptyField extends Error {
  constructor(book, field) {
    super(`Empty field '${field}' in book: ${book.name}`);
    this.name = "EmptyField";
  }
}

books.forEach((book) => {
  try {
    if (!book.author) {
      throw new EmptyField(book, "author");
    } 
    if (!book.price) {
      throw new EmptyField(book, "price");
    }
  
      const listItem = document.createElement("ul");

      listItem.innerHTML = `
        <li>${book.author}</li>
        <li>${book.name}</li>
        <li>${book.price}</li>`;

      listContainer.prepend(listItem);

} catch (err) {
    if (err.name === "EmptyField") {
      console.warn(err);
    } else {
      throw err;
    }
  }
});





