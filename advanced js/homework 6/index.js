/*
це означає, що код може виконуватися не в послідовному порядку від початку до кінця. Замість цього код може виконуватися паралельно або після завершення інших дій.
*/


const buttonFindMe = document.querySelector("button");

const findFunction = async () => {
    try{
        const {data} = await axios('https://api.ipify.org/?format=json');
        const {ip} = data;

        await axios(`http://ip-api.com/json/${ip}`)
            .then(({data}) => {
                document.body.insertAdjacentHTML("beforeend",`
                <ul>
                     <li>timezone:  ${data.timezone}</li>
                     <li>country:  ${data.country}</li>
                     <li>region:  ${data.regionName}</li>
                     <li>city:  ${data.city}</li>
                </ul>`)
            })
    }catch (err) {
        console.log(err)
    }
};


buttonFindMe.addEventListener("click", (event) =>{
    event.preventDefault();
    findFunction();
});

