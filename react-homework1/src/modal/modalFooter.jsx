
function ModalFooter({ buttons }) {
  return (
    <div className="modal-footer">
      {buttons}
    </div>
  );
}

export default ModalFooter;