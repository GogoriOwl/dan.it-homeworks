import './modal.css'
import React from 'react';

function ModalWrapper({ children, closeModal }) {

  function handleOverlayClick(e) {
    if (e.target === e.currentTarget) {
      closeModal();
    }
  }

  return (
    <div className="modal-background-screen" onClick={handleOverlayClick}>
      <div className="modal-wrapper">
        {children}
      </div>
    </div>
  );
}
 
export default ModalWrapper;