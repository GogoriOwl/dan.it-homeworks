
function ModalHeader({ button }) {
  return (
    <div className="modal-header">
      {button}
    </div>
  );
}

export default ModalHeader;