import "./modal.css"

function ModalBody({ title, text }) {
  return (
    <div className="modal-body">
      <h1>{title}</h1>
      <p>{text}</p>
    </div>
  );
}

export default ModalBody;