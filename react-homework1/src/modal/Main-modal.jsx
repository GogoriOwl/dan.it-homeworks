import React from 'react';
import ModalHeader from './modalHeader';
import ModalWrapper from './modalWrapper';
import ModalBody from './modalBody';
import ModalFooter from './modalFooter';
import '../button/button.css'
import svg from '../assets/Icon.svg';

export function ModalOne({ closeModal }) {
  return (
    <ModalWrapper closeModal={closeModal}>
      <ModalHeader button={<button onClick={closeModal} className='close-button' type='button'><img src={svg} alt="close" /></button>} />
      <ModalBody title="Product Delete!" text='By clicking "Yes, delete" button Product Name will be deleted' />
      <ModalFooter buttons={
        <>
          <button onClick={closeModal} className='btn' type='button'>No, cancel</button>
          <button className='btn delete-prod' type='button'>Yes, delete</button>
        </>
      } />
    </ModalWrapper>
  );
}

export function ModalTwo({ closeModal }) {
  return (
    <ModalWrapper closeModal={closeModal}>
      <ModalHeader button={<button onClick={closeModal} className='close-button' type='button'><img src={svg} alt="close" /></button>} />
      <ModalBody title="Add Product 'NAME'" text='Description for you product' />
      <ModalFooter buttons={
        <>
          <button className='btn' type='button'>ADD TO FAVORITE</button>
        </>
      } />
    </ModalWrapper>
  );
}


