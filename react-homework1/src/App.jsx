import { useState } from 'react'
import Button from './button/button'
import  {ModalOne} from './modal/Main-modal';
import  {ModalTwo} from './modal/Main-modal';

function App() {
  const [activeModal, setActiveModal] = useState(null);

  function showModalOne() {
    setActiveModal('modalOne');
  }

  function showModalTwo() {
    setActiveModal('modalTwo');
  }

  function closeModal() {
    setActiveModal(null);
  }

  return (
    <>
      {activeModal === 'modalOne' && <ModalOne closeModal={closeModal} />}
      {activeModal === 'modalTwo' && <ModalTwo closeModal={closeModal} />}

      <button onClick={showModalOne} className="btn" disabled={activeModal !== null}>
        Open first Modal
      </button>
      <button onClick={showModalTwo} className="btn" disabled={activeModal !== null}>
        Open second Modal
      </button>
    </>
  );
}

export default App;
