import closeButton from '../assets/Icon.svg'

function CloseButton(){
  return(
  <img className='.close-button' type='button' src="{closeButton}" alt="" />
  )
}

export default CloseButton;