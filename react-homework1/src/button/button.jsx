import "./button.css"


function Button({className, type, onClick, children}){

  return(
    <button className="btn" type="button">{children}</button>
  )
}

export default Button;