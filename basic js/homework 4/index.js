'Use strict'
/* 1. цикл - це конструкція, яка дозволяє виконувати певну послідовність інструкцій кілька разів до досягнення певної умови щоб автоматизувати операції, які потребують повторення.

 2. Цикл "for": Цей цикл використовується для повторення певної дії певну кількість разів.
  Цикл "while": Цей цикл виконує блок коду, поки задана умова залишається істинною.
 Цикл "do-while": Цей цикл виконує блок коду, а потім перевіряє умову. Він завжди виконається принаймні один раз.

  3. відмінність між циклами do-while та while полягає у способі перевірки умови.
  Цикл while спершу перевіряє умови і лиш потім виконує код тоді як do while робить навпаки завдяки чому код виконується хоча б 1 раз.
 */



let numberOne

let numberTwo

let evenNumber

do{

  numberOne = prompt('enter the first number');

} while(isNaN(numberOne) || !numberOne)

do{

  numberTwo = prompt('enter the second number');

} while(isNaN(numberTwo) || !numberTwo)

if (numberOne < numberTwo){

  for(let i = numberOne; i <= numberTwo; i++){
    console.log(i);
  }

} else{
  for(let i = numberTwo; i <= numberOne; i++){
    console.log(i);
  }
}

do{
  evenNumber = prompt("type in an even number")
} while(isNaN(evenNumber) || !evenNumber || evenNumber % 2 !== 0)
