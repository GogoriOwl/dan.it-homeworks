"use strict";

/*
1. Для того щоб оголосити змінну потрібно використати та написати одну зі змінних (таких як var, const,  або ж let) після чого через пробіл написати назву змінної та після оператору присвоєння (=) надати їй значення.

Наприклад:

const age = 23;

2. Рядок string - один з типів даних.
Створити його можна використовуючи різні види лапок (”” '' ``) у нашій змінній
Наприклад:

const name = “Arthur”;

Або ж огорнувши у лапки текст в console.log

Наприклад:

console.log(` `);

3. Для того щоб перевірити тим даних потрібно в console.log() перед нашою змінною написати ʼtypeof’
Наприклад:

const name = “Arthur”;

const.log(typeof name);

4. У даному випадку результатом буде 11 адже даний оператор працює таким чином що маючи хоча б 1 тим даних string (яким є ʼ1ʼ) він перетворю і другий тим даних також у string (виконуючи неявне перетворення) і лиш потім додає ці рядки.

*/



const age = 23;

console.log(typeof age);

const name = `Arthur`;

const lastName = `Zhuk`;

const message = `my name is: ${name}, my last name is: ${lastName}`;

console.log(message)

const number = 369;

console.log(`number: ${number}`)


