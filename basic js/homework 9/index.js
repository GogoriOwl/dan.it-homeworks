'use strict'
/* 
1.Document Object Model (DOM) - це програмний інтерфейс, який надає структуроване представлення веб-сторінки або HTML-документа. 
У звичайному HTML-документі DOM можна уявити як дерево об'єктів, де кожен елемент документа (такий як тег, атрибут, текстовий вузол) представлений об'єктом, і ці об'єкти утворюють ієрархію від кореня документа до всіх його елементів.

2.Навідміну від innerText, innerHtml може виконувати код якщо він попаде з даними 
innerText же у свою чергу цього не робить та сприймає все як звичайний текст.

3. 1.getElementById
   2.getElementsByClassName
   3.getElementsByTagName
   4.querySelector
   5.querySelectorAll

   querySelector та querySelectorAll найновіші та найефективнішi.

4.NodeList є стандартним об'єктом DOM, який представляє собою колекцію вузлів, таких як елементи або атрибути. Його можна отримати, наприклад, використовуючи document.querySelectorAll.
HTMLCollection - це також колекція DOM, але вона обмежена елементами, які мають атрибут name або id. Зазвичай вона повертається за допомогою методів, таких як document.getElementsByName або document.getElementById.

NodeList - живий, тобто будь-які зміни в DOM автоматично відображаються в NodeList. Якщо, наприклад, ви видаляєте елемент, який входить в NodeList, ця колекція автоматично оновлюється.
HTMLCollection - також живий, але тільки в деяких випадках. Наприклад, коли ви використовуєте getElementsByName або getElementById, зміни в DOM впливають на HTMLCollection. Однак HTMLCollection не завжди оновлюється автоматично при деяких інших операціях.

*/

const featureElemFirst = document.querySelectorAll('.feature');

console.log(featureElemFirst);

const featureElemSecond = document.getElementsByClassName('feature');

console.log(featureElemSecond)



featureElemFirst.forEach(elem => {
  elem.style.textAlign = 'center'
})

const secondTypeHeader = document.querySelectorAll('h2');

secondTypeHeader.forEach(elem => {
  elem.innerText = 'Awesome feature'
})

const featureTitle = document.querySelectorAll('.feature-title')

featureTitle.forEach(elem => {
  elem.outerText += '!'
})