/*
1.setTimeout: Виконує заданий код один раз через певний інтервал часу.
setInterval: Виконує заданий код періодично через певний інтервал часу, поки не буде скасований.

2.Для припинення виконання функції, яка була запланована для виклику з використанням setTimeout або setInterval, ви можете скористатися методом clearTimeout
*/

const textToChange = document.querySelector(".interval-text-change");
const changeButton = document.querySelector(".text-change-button");
let intervalTextCgange = null;
const timerText = document.querySelector(".timer-text");
const timerButton = document.querySelector(".timer-button");
let intervalTimer = null;

window.addEventListener("click", (event) => {

  if(intervalTimer) {
    return;
  }

  if(event.target === changeButton) {

    intervalTextCgange =  setTimeout(() => {
      textToChange.innerText = "operation succsesful"
    }, 3000);
   
  } if(event.target === timerButton){

    intervalTimer = setInterval(() => {
      if(timerText.innerText > 1) {
        timerText.innerText--
      } else{
        timerText.innerText = "Зворотній відлік завершено";
        clearInterval(intervalTimer);
      }
    
    }, 1000)
  }
})