const changeThemeButton = document.querySelector("#change-theme");
const pageTheme = sessionStorage.getItem("dark-mode");

/*
1. localStorage: Дані зберігаються постійно (поки не будуть явно видалені або очищені).
  sessionStorage: Дані зберігаються лише протягом однієї сесії перегляду. Якщо ви закриєте вкладку або веб-переглядач, дані будуть видалені.

  localStorage: Дані доступні для всіх вкладок і вікон в тому ж самому домені.
  sessionStorage: Дані доступні лише для поточної вкладки або вікна. Вони не доступні для інших вкладок або вікон, які відкриті в тому ж самому домені.

  localStorage: Дані залишаються збереженими навіть після закриття браузера. Вони можуть бути очищені лише вручну користувачем або за допомогою скриптів.
  sessionStorage: Дані автоматично видаляються після закриття вкладки або вікна, на якій вони були збережені. Вони не залишаються після закриття веб-сайту або браузера.
2. Ці веб-сховища не призначені для зберігання чутливої інформації, оскільки дані в них можуть бути доступні через JavaScript і зазнають ризику від перехоплення атаками типу cross-site scripting (XSS). Замість цього, використовуйте механізми зберігання, спеціально призначені для паролів, такі як localStorage або sessionStorage.

3.Коли сеанс браузера завершується (наприклад, користувач закриває всі вкладки або веб-переглядач), дані, збережені в sessionStorage, будуть автоматично видалені. 
*/


changeThemeButton.addEventListener("click", () => {
const buttons = document.querySelectorAll(".btn");

const body = document.querySelector("body");
body.classList.toggle("dark-mode-body");

const text = document.querySelectorAll(".sub-header, .logo-name-footer")
text.forEach((el) => {
  el.classList.toggle("dark-mode-text")
})

buttons.forEach((button) => {
  button.classList.toggle("dark-mode-button");
  button.classList.remove("sign-in");
})

if(body.classList.contains("dark-mode-body")){
  localStorage.setItem("dark-mode", "on");
} else {
  localStorage.setItem("dark-mode", "off");
}

})

if(localStorage.getItem("dark-mode") === "on") {
  const keepDarkMode = function(){
    changeThemeButton.click();
  }

  keepDarkMode();
}




