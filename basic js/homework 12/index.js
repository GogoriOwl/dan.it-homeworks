"use strict"


/*

1.використовуючи властивість event.code в обробнику події клавіатури. Властивість event.code повертає рядок, що представляє фізичний код клавіші.

window.addEventListener("keydown", (event) => {console.log(event.code)}

2. 1.event.code: Повертає фізичний код клавіші, який не залежить від розкладки клавіатури.

   2.event.key: Повертає значення клавіші, яке залежить від розкладки клавіатури та може враховувати стан клавіші Caps Lock, Shift

3.  Три основні події, пов'язані з клавішами, це keydown, keyup та keypress.

keydown. Ця подія виникає, коли клавіша натискається.
keyup. Ця подія виникає, коли клавіша відпускається.
keypress. Ця подія виникає, коли клавіша, яка представляє символ, натискається.
*/

const button = document.querySelector("button")

window.addEventListener("keydown", (event) => {
console.log(event.code)

  const activeButton = document.querySelector(".active")

  if(activeButton){
    activeButton.classList.remove("active")
  }

 let userButton = document.querySelector(`#${event.code.toLowerCase()}-btn`);

 console.log(userButton)

  if(userButton){
    userButton.classList.add("active");
  }

})




