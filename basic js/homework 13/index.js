/*
1.Метод event.preventDefault() у JavaScript використовується для скасування стандартної дії, яка пов'язана з подією. Це означає, що виклик цього методу перешкоджає стандартній поведінці, яка зазвичай виникає при виникненні події.

2.Прийом делегування подій - це підхід в програмуванні, коли ми встановлюємо обробники подій на батьківський елемент, а не на кожен окремий дочірній елемент. Коли подія спрацьовує на дочірньому елементі, вона спочатку потрапляє до батьківського елемента, і тоді може бути оброблена в обробнику подій батьківського елемента.

3. 1.click
   2.keydown
   3.keyup
   4.dblclick
   5.focus
   6.blur
   7.focus
   8.submit


*/

const btnContainer = document.querySelector(".centered-content");

btnContainer.addEventListener("click", (event) => {

  const tabsContainer = document.getElementById("tabs-container");

  if (!event.target.id.endsWith("-btn")) {
    return;
  }

  const expectedSuffix = "-btn";

  let activeButton = document.querySelector(".active");
  let activeContent = document.querySelector(".active-content");


  if (activeButton) {
    activeButton.classList.remove("active");
  }

  if (activeContent) {
    activeContent.classList.remove("active-content");
  }

  if (event.target.id.endsWith(expectedSuffix)) {
    event.target.classList.add("active");

    let baseId = event.target.id.slice(0, -expectedSuffix.length);
    let relatedContentId = `${baseId}-related`;

    let relatedContent = document.getElementById(relatedContentId);

    if (relatedContent) {
      relatedContent.classList.add("active-content");
    } 

    const allContent = document.querySelectorAll(".content");
    allContent.forEach(content => {
      if (content.id !== relatedContentId) {
        content.classList.remove("active-content");
      }
    });
  }


});
