/*
1. Метод об'єкту - це функція, яка є властивістю об'єкта. У JavaScript об'єкти можуть містити не лише дані (які називають властивостями), але й функції, які виконують певні дії з цими даними. Коли функція прикріплена до об'єкта як його властивість, вона стає методом цього об'єкта.

2. 1.Рядки
  2.Числа
  3.Логічні значення 
  4.Масиви (Arrays)
  5.Інші об'єкти 
  6.Функції (Functions)
  7.Null і Undefined: 

3. Це означає, що коли змінна об'єкту створюється або присвоюється іншій змінній, не копіюється сам об'єкт, а його посилання.
*/





const obj = {
  name : 'book',
  price : 30,
  discount : 13.20,
}

const displayPrice = function(){
  console.log(obj.price - obj.discount);
}

displayPrice();








const createUser = function(){
  let name = prompt("enter your name");
  let age = prompt('enter your age');

  const user = {
    name,
    age, 
  }

return user;
}

const userOne = createUser();

const userMessage = function(){
  alert(`Hello, my name is ${userOne.name} and I'm ${userOne.age} years old`)
}

userMessage();
