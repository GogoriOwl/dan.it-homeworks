'use strict'

/*
1. Це механізм, який дозволяє вам реагувати на дії користувача чи інші події в середовищі виконання скрипту, такому як веб-браузер. Події можуть виникати при різних обставинах, таких як клік мишею, натискання клавіші, завантаження сторінки, зміна розміру вікна і т.д.

2. 1.click
   2.dblclick
   3.mouseover
   4.mousedown  
   5.mouseup

3.Подія "contextmenu" в JavaScript виникає, коли користувач натискатиме праву кнопку миші для виклику контекстного меню. Ця подія дозволяє вам визначити власну логіку для обробки відображення контекстного меню або виконання дій, пов'язаних з правим кліком миші.

*/

const buttonParagraph = document.querySelector("#btn-click");

const createParagraph = function() {

  const newParagraph = document.createElement("p");

  newParagraph.innerText = "New Paragraph";

  document.querySelector("#content > p").append(newParagraph);
  
}

buttonParagraph.addEventListener("click", createParagraph);

const inputButton = document.createElement("button");
inputButton.innerText = 'Create input';
inputButton.setAttribute(
  "style",
  "display: block; margin-left: 41%; margin-top: 5px;"
)

const contentSection = document.querySelector('#content');

contentSection.append(inputButton);

const createInput = function(){

  const newInput = document.createElement("input")

  const attributes = {
    placeholder: "enter your name",
    name: "text",
    type: "name",
    style: "display: block",
  }

 Object.assign(newInput, attributes);

 contentSection.append(newInput)

}

inputButton.addEventListener("click", createInput)

console.log(createInput)

