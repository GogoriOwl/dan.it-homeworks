"use strict"
/*
1. У JavaScript рядок можна створити, використовуючи лапки (одинарні або подвійні) навколо тексту.

let string = "it's a string";

2.У більшості випадків різниці немає.
Окрім як відображення апострофів (тоді краще використовувати подвійні лапки (““) або ж доведеться використовувати спеціальний символ для відображення (/).

Зворотні лапки дозволяють створювати шаблонні рядки, в яких можна вставляти змінні або вирази

3.За допомогою метода .localeCompare()


4.Date.now() у JavaScript повертає поточний час в мілісекундах

5.Date.now() просто повертає поточний час у мілісекундах у вигляді числа, тоді як new Date() створює новий об'єкт Date, який представляє поточну дату та час.
*/


const isPalindrome = function(){

  let userWord = prompt("enter your word");

  let reversedWord = "";

  for (let i = userWord.length - 1; i >= 0; i--){
    reversedWord += userWord[i];
  }

  return console.log(reversedWord.localeCompare(userWord) === 0);
}

isPalindrome()




const lengthCheck = function(){
  const userString = prompt('enter your string');

  const neededString = 20;

  return userString.length <= neededString
 
}

console.log(lengthCheck())

function calculateAge() {

  let birthDate = new Date(prompt("Введіть дату народження у форматі YYYY-MM-DD:"));

  let currentDate = new Date();

  let age = currentDate.getFullYear() - birthDate.getFullYear();

  return age;
}

console.log(calculateAge())