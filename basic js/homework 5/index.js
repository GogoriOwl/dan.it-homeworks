'use strict'
//1
// Для того щоб створити функцію з першої потрібно оголосити. Щоб оголосити функцію є 2 різних способи. 
// У 1-му способі ми оголошуємо зміну let і вже в ній робимо функцію. 
// let funcOne = function(){}
// У 2-му способі ми одразу пишемо ключове слово function після чого відкриваємо спочатку звичайні дужки а потім фігурні.
// function funcTwo(){}

//2
// Оператор return в JavaScript використовується для повернення значення з функції. Коли функція викликається і доходить до оператора return, вона завершує своє виконання і повертає значення назад туди, де було зроблено виклик цієї функції.

// const getAgeAccess = function (age) {
// //   if (age ›= 18) { return 'Welcome to the content'
// //   } else {
// //   return 'Access denied'
// //   }
// // }

//3
// Параметри - це змінні, які оголошуються у визначенні функції. Вони виступають як місця для прийому значень, які будуть використовуватися всередині функції. 

// function showGreetingwithName (name) {
//   console.log(Hello from $ {name}*);
//   вчанням, ми повинні вказати його після
//   showGreetingwithName ('Bob ") ;

//4

// function action() {
//   функція();
// }

// function greeting() {
//   console.log("Привіт з функції!");
// }

// action(greeting)
// У цьому прикладі функція виконатиДію приймає іншу функцію як параметр функція, а потім просто викликає цю передану функцію всередині себе.



//1

let firstNumber

let secondNumber

const enterNumbers = function(){
  do{
    firstNumber = +prompt('enter the first number')
 } while(isNaN(firstNumber) || !firstNumber)
 
 do{
  secondNumber = +prompt('enter the second number')
 } while(isNaN(secondNumber) || !secondNumber)
}

const divisionResult = function(){
  console.log(firstNumber / secondNumber); 
}

const division = function(){
  enterNumbers();
  divisionResult();
}

division();

//2

let numberOne

let numberTwo

let mathOperation

const userNumbers = function(){
  do{
    numberOne = +prompt('enter the first number')
 } while(isNaN(numberOne) || !numberOne)
 
 do{
    numberTwo = +prompt('enter the second number')
 } while(isNaN(numberTwo) || !numberTwo)
}

const userOperator = function(){
   mathOperation = prompt("enter a math operation");
}

const calc = function(){
  switch(mathOperation){
    case "+" : {
      console.log(numberOne + numberTwo);
      break
    }
  
    case "-" : {
      console.log(numberOne - numberTwo);
      break
    }
  
    case "/" : {
      console.log(numberOne / numberTwo);
      break
    }
  
    case "*" : {
      console.log(numberOne * numberTwo);
      break
    }
  
    default : {
      alert("It's not a mathimatical operation")
    }
  }
}

const mathOperations = function(){
  userNumbers();
  userOperator();
  calc();
}

mathOperations();


