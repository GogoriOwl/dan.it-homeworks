'use strict'

/*
1. 1.document.createElement(tagName)
   2.document.createTextNode(text)
   3.element.insertAdjacentHTML

2. 1.const navigation = document.querySelector('.navigation');
    2. navigation.remove();

3. 1.insertBefore
   2.insertAdjacentHTML
   3. element.prepend()
   4. element.append()
*/

const link = document.createElement('a');

link.innerText = 'Learn more';
link.setAttribute("href", "#");

document.querySelector("footer > p").append(link);
link.style.color = 'grey';

const gradeOptions = document.createElement('select');
gradeOptions.setAttribute("id", "rating");

document.querySelector('main').prepend(gradeOptions)

const gradeOptionOne = document.createElement('option');gradeOptionOne.value = '1';
gradeOptionOne.text = '1 star';

const gradeOptionTwo = document.createElement('option');gradeOptionTwo.value = '2';
gradeOptionTwo.text = '2 stars';

const gradeOptionThree = document.createElement('option');gradeOptionThree.value = '3';
gradeOptionThree.text = '3 stars';

const gradeOptionFour = document.createElement('option');gradeOptionFour.value = '4';
gradeOptionFour.text = '4 stars';

gradeOptions.add(gradeOptionOne);
gradeOptions.add(gradeOptionTwo);
gradeOptions.add(gradeOptionThree);
gradeOptions.add(gradeOptionFour);

